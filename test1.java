import java.util.Arrays;
import java.util.Scanner;
public class test1 {
	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
		int n = kb.nextInt();
		int num[] = new int[n];
		if(2<=n && n<=1000) {
			for(int i=0;i<n;i++) {
				num[i] = kb.nextInt();
			}
			Arrays.sort(num);
			for(int j=0;j<num.length;j++) {
				if(num[j] > 0) {
					int temp = num[j];
					num[j] = num[0];
					num[0] = temp;
					break;
				}
			}
			for(int k=0;k<num.length;k++) {
				System.out.print(num[k]);
			}
		}else {
			System.out.println("Please enter new number");
		}
	}
}
